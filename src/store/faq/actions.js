import Vue from 'vue'
export function getAllCount (context) {
  return Vue.prototype.$api.faq.getAllCount()
}
export function getAllFaq (context, data) {
  return Vue.prototype.$api.faq.getAllFaq(data)
}
export function addFaq (context, data) {
  return Vue.prototype.$api.faq.addFaq(data)
}

export function removeFaq (context, data) {
  return Vue.prototype.$api.faq.removeFaq(data)
}
export function editFaq (context, data) {
  return Vue.prototype.$api.faq.editFaq(data)
}
