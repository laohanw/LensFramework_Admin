#!/bin/bash
BASEURL=$1
SERVERIP=$2
QAULITYPATH=$3

yarn run test:e2e:CI --config baseUrl=${BASEURL}
ls test/cypress/screenshots
ls test/cypress/videos
cd test
tar czf cypress.tar.gz cypress/{screenshots,videos}/*
expect << __EOF
spawn ssh -p 22 root@${SERVERIP}
expect {
  "*yes/no)?" {
    send "yes\n";
    exp_continue
  }
  "*assword:" {
    send "pAssW0rd!\n"
  }
}
expect "*]#" {send "mkdir -p ${QAULITYPATH}\n"}
expect "*]#" {send "logout\n"}
expect eof

spawn scp cypress.tar.gz root@${SERVERIP}:${QAULITYPATH}
expect "*assword:" {send "pAssW0rd!\n"}
expect eof

spawn ssh -p 22 root@${SERVERIP}
expect {
  "*assword:" {
    send "pAssW0rd!\n"
  }
}
expect "*]#" {send "cd $QAULITYPATH\n"}
expect "*]#" {send "tar -zxvf cypress.tar.gz\n"}
expect "*]#" {send "rm -rf cypress.tar.gz\n"}
expect "*]#" {send "logout\n"}
expect eof
__EOF

rm -rf cypress.tar.gz