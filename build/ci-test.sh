#!/bin/bash
SERVERIP=$1
QAULITYPATH=$2

yarn run test:jest:CI
ls test/jest/coverage
cd test/jest
tar czf jest.tar.gz coverage/*
expect << __EOF
spawn ssh -p 22 root@${SERVERIP}
expect {
  "*yes/no)?" {
    send "yes\n";
    exp_continue
  }
  "*assword:" {
    send "pAssW0rd!\n"
  }
}
expect "*]#" {send "mkdir -p ${QAULITYPATH}\n"}
expect "*]#" {send "logout\n"}
expect eof

spawn scp jest.tar.gz root@${SERVERIP}:${QAULITYPATH}
expect "*assword:" {send "pAssW0rd!\n"}
expect eof

spawn ssh -p 22 root@${SERVERIP}
expect {
  "*assword:" {
    send "pAssW0rd!\n"
  }
}
expect "*]#" {send "cd $QAULITYPATH\n"}
expect "*]#" {send "tar -zxvf jest.tar.gz\n"}
expect "*]#" {send "rm -rf jest.tar.gz\n"}
expect "*]#" {send "logout\n"}
expect eof

__EOF